#!/bin/bash -xe

VERSION=1

if [ "${1}" == "-version" ] ||  [ "${1}" == "--version" ] || [ "${1}" == "-v" ] ; then
    echo $VERSION
    exit 0
fi


SUBJECT=$1
FS_VERSION=$2
OUTFILE=$3

case $FS_VERSION in
    5.1)
    SETUP=freesurfer5_setup.sh
    ;;
    5.3-HCP)
    SETUP=freesurfer53-HCP_setup.sh
    ;;
    5.3-HCP-patch)
    SETUP=freesurfer53-patch_setup.sh
    ;;
    *)
    echo "I don't recognize FreeSurfer version $FS_VERSION"
    exit 1
    ;;
esac

export SUBJECTS_DIR=`pwd`

#check that input exists
if [ ! -d "${SUBJECTS_DIR}/${SUBJECT}" ]; then
	echo "Directory $SUBJECT must exist within SUBJECTS_DIR $SUBJECTS_DIR"
	exit 1
fi

# Set up Freesurfer
source /data/CNDA/pipeline/scripts/$SETUP
# source /data/CNDA/pipeline/scripts/$SETUP

###### OUTPUT ALL STATS TRANSPOSED #######

#####ASEG######
asegstats2table --subjects ${SUBJECT} --delimiter comma --all-segs --transpose -t ${SUBJECT}_aseg.csv

#####APARC######
#volumes LH
aparcstats2table --hemi lh --subjects ${SUBJECT} --delimiter comma --measure volume --transpose -t ${SUBJECT}_vol_lh.csv

#volumes RH
aparcstats2table --hemi rh --subjects ${SUBJECT} --delimiter comma --measure volume --transpose -t ${SUBJECT}_vol_rh.csv


#thicknesses LH
aparcstats2table --hemi lh --subjects ${SUBJECT} --delimiter comma --measure thickness --transpose -t ${SUBJECT}_thick_lh.csv

#thicknesses RH
aparcstats2table --hemi rh --subjects ${SUBJECT} --delimiter comma --measure thickness --transpose -t ${SUBJECT}_thick_rh.csv

###### PUT EVERYTHING TOGETHER #######
#cut off the top row of all the files except the first one, then cat them together
cp ${SUBJECT}_aseg.csv ${SUBJECT}_FS_stats_transposed.csv
tail -n +2 ${SUBJECT}_vol_lh.csv >> ${SUBJECT}_FS_stats_transposed.csv
tail -n +2 ${SUBJECT}_vol_rh.csv >> ${SUBJECT}_FS_stats_transposed.csv
tail -n +2 ${SUBJECT}_thick_lh.csv >> ${SUBJECT}_FS_stats_transposed.csv
tail -n +2 ${SUBJECT}_thick_rh.csv >> ${SUBJECT}_FS_stats_transposed.csv

###### TRANSPOSE THE OUTPUT #######
python -c "import sys; print('\n'.join(','.join(cols) for cols in zip(*(line.strip().split(',') for line in sys.stdin.readlines() if line.strip()))))" < ${SUBJECT}_FS_stats_transposed.csv > $OUTFILE

###### HEADER TRANSFORMATIONS #######
sed -i -e '1 s/Measure:volume/Session/' -e '1 s/-/_/g' -e '1 s/3rd/third/' -e '1 s/4th/fourth/' -e '1 s/5th/fifth/' -e '1 s/EstimatedTotal//' $OUTFILE

###### CLEANUP #######
rm ${SUBJECT}_aseg.csv ${SUBJECT}_vol_lh.csv ${SUBJECT}_vol_rh.csv ${SUBJECT}_thick_lh.csv ${SUBJECT}_thick_rh.csv ${SUBJECT}_FS_stats_transposed.csv
