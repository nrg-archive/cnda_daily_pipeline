<?xml version="1.0" encoding="UTF-8"?>
<Pipeline xmlns="http://nrg.wustl.edu/pipeline" xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/pipeline ..\..\schema\pipeline.xsd" xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils">
    <name>RegisterMRToAtlas</name>
    <location>RegisterMR</location>
    <description>Register MR scans to specified atlas. Add registered files at scan level.</description>
    <resourceRequirements>
        <property name="DRMAA_JobTemplate_JobCategory">atlas_register_q</property>
    </resourceRequirements>
    <documentation>
        <authors>
            <author>
                <lastname>Jon</lastname>
                <firstname>Christensen</firstname>
            </author>
            <author>
                <lastname>Yi</lastname>
                <firstname>Su</firstname>
            </author>
            <author>
                <lastname>Mohana</lastname>
                <firstname>Ramaratnam</firstname>
            </author>
            <author>
                <lastname>Flavin</lastname>
                <firstname>John</firstname>
                <contact>
                    <email>flavinj@mir.wustl.edu</email>
                </contact>
            </author>
        </authors>
        <version>20150310</version>
        <input-parameters>
            <parameter>
                <name>scanIds</name>
                <values>
                    <schemalink>xnat:imageSessionData/scans/scan/ID</schemalink>
                </values>
                <description>Ids of scans to register</description>
            </parameter>
            <parameter>
                <name>scantype</name>
                <values>
                    <csv>MPRAGE,MPRAGE GRAPPA2,MPRAGE GRAPPA2 repeat</csv>
                </values>
                <description>Scantype(s) of the T1 scan</description>
            </parameter>
            <parameter>
                <name>atlas</name>
                <values>
                    <csv>711-2B</csv>
                </values>
                <description>Study-representative atlas to which MRs will be registered.</description>
            </parameter>
            <parameter>
                <name>autorun</name>
                <values>
                    <csv>0,1</csv>
                </values>
                <description>Set to 1 if using autorun, or 0 if launching manually</description>
            </parameter>
            <parameter>
                <name>format</name>
                <values>
                    <csv>DICOM</csv>
                </values>
                <description>Format of input files. DICOM or NIFTI.</description>
            </parameter>
        </input-parameters>
    </documentation>
    <outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/LOGS/',/Pipeline/parameters/parameter[name='label']/values/unique/text())^</outputFileNamePrefix>
    <loop id="scanId" xpath="^/Pipeline/parameters/parameter[name='scanIds_loopInput']/values/list^"/>
    <parameters>
        <!-- Fixed Section -->
        <parameter>
            <name>workdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='label']/values/unique/text())^</unique>
            </values>
        </parameter>
        <parameter>
            <name>4dfpdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/4DFP')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>rawdir</name>
            <values>
                <unique>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/RAW')^</unique>
            </values>
        </parameter>
        <parameter>
            <name>scantype_csv</name>
            <values>
                <unique>^if (/Pipeline/parameters/parameter[name='autorun']/values/unique/text()='1') then if (count(/Pipeline/parameters/parameter[name='scantype']/values/list) >0) then string-join(/Pipeline/parameters/parameter[name='scantype']/values/list,',') else /Pipeline/parameters/parameter[name='scantype']/values/unique/text() else ''^</unique>
                <!-- <unique>^if (count(/Pipeline/parameters/parameter[name='scantype']/values/list) >0) then string-join(/Pipeline/parameters/parameter[name='scantype']/values/list,',') else /Pipeline/parameters/parameter[name='scantype']/values/unique/text()^</unique> -->
            </values>
            <description>If using autorun, the csv of scan types will be converted to a list. We must convert it back.</description>
        </parameter>
        <parameter>
            <name>scanIds_loopInput</name>
            <values>
                <list>^if (/Pipeline/parameters/parameter[name='autorun']/values/unique/text()='1') then fileUtils:GetScanIdsByType(/Pipeline/parameters/parameter[name='host']/values/unique/text(),/Pipeline/parameters/parameter[name='user']/values/unique/text(),/Pipeline/parameters/parameter[name='pwd']/values/unique/text(),/Pipeline/parameters/parameter[name='id']/values/unique/text(),/Pipeline/parameters/parameter[name='scantype_csv']/values/unique/text()) else /Pipeline/parameters/parameter[name='scanIds']/values/list/text()^</list>
            </values>
            <description>If using autorun, get the list of scanids from the scantypes. If launching manually, respect the user’s input.</description>
        </parameter>
    </parameters>
    <steps>
        <step id="MKDIR" description="Prepare folder structure" >
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='4dfpdir']/values/unique/text()^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^/Pipeline/parameters/parameter[name='rawdir']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="MKDIR_SCANS" description="Prepare folder structure" >
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^concat(/Pipeline/parameters/parameter[name='4dfpdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId))^</value>
                </argument>
            </resource>
            <resource name="mkdir" location="commandlineTools">
                <argument id="p"/>
                <argument id="dirname">
                    <value>^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId))^</value>
                </argument>
            </resource>
        </step>
        <step id="GET_MR" description="Copy MPRAGE DICOM data"  workdirectory="^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId))^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="method">
                    <value>GET</value>
                </argument>
                <argument id="absolutePath"/>
                <argument id="batch"/>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/scans/',PIPELINE_LOOPON(scanId),'/resources/',/Pipeline/parameters/parameter[name='format']/values/unique/text(),'/files"')^</value>
                </argument>
            </resource>
        </step>
        <step id="CONVERT_DICOM" description="Create 4dfp file from MPRAGE" workdirectory="^concat(/Pipeline/parameters/parameter[name='4dfpdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId))^" precondition="^/Pipeline/parameters/parameter[name='format']/values/unique/text()='DICOM'^">
            <resource name="dcm_to_4dfp" location="nilResources">
                <argument id="q"/>
                <argument id="b">
                    <value>^concat(/Pipeline/parameters/parameter[name='label']/values/unique/text(),'_',PIPELINE_LOOPON(scanId),'_mpr')^</value>
                </argument>
                <argument id="file">
                    <value>^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId),'/*')^</value>
                </argument>
            </resource>
        </step>
        <step id="CONVERT_NIFTI" description="Create 4dfp file from MPRAGE" workdirectory="^concat(/Pipeline/parameters/parameter[name='4dfpdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId))^" precondition="^/Pipeline/parameters/parameter[name='format']/values/unique/text()='NIFTI'^">
            <resource name="nifti_4dfp" location="nilResources">
                <argument id="nii_to_4dfp" />
                <argument id="outfile">
                    <value>^concat(/Pipeline/parameters/parameter[name='label']/values/unique/text(),'_',PIPELINE_LOOPON(scanId),'_mpr')^</value>
                </argument>
                <argument id="infile">
                    <value>^concat(/Pipeline/parameters/parameter[name='rawdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId),'/*')^</value>
                </argument>
            </resource>
        </step>
        <step id="REGISTER" description="Register MPRAGE to atlas space" workdirectory="^concat(/Pipeline/parameters/parameter[name='4dfpdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId))^">
            <resource name="MRITOTARGET" location="RegisterMR/resources" >
                <argument id="input">
                    <value>^concat(/Pipeline/parameters/parameter[name='label']/values/unique/text(),'_',PIPELINE_LOOPON(scanId),'_mpr')^</value>
                </argument>
                <argument id="atlas">
                    <value>^/Pipeline/parameters/parameter[name='atlas']/values/unique/text()^</value>
                </argument>
            </resource>
        </step>
        <step id="UPLOAD" description="Upload MPRAGE data" workdirectory="^concat(/Pipeline/parameters/parameter[name='4dfpdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId))^">
            <resource name="XnatDataClient" location="xnat_tools">
                <argument id="sessionId">
                    <value>^fileUtils:getJSESSION('DUMMY')^</value>
                </argument>
                <argument id="method">
                    <value>PUT</value>
                </argument>
                <argument id="remote">
                    <value>^concat('"',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'/scans/',PIPELINE_LOOPON(scanId),'/resources/REGISTERED_',/Pipeline/parameters/parameter[name='atlas']/values/unique/text(),'/files?overwrite=true&amp;format=4DFP&amp;event_id=',/Pipeline/parameters/parameter[name='workflowid']/values/unique/text(),'&amp;reference=',/Pipeline/parameters/parameter[name='4dfpdir']/values/unique/text(),'/',PIPELINE_LOOPON(scanId),'"')^</value>
                </argument>
            </resource>
        </step>
        <step id="CLEANUP" description="Remove build files" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
            <resource name="rm" location="commandlineTools">
                <argument id="file">
                    <value>^/Pipeline/parameters/parameter[name='rawdir']/values/unique/text()^</value>
                </argument>
                <argument id="r"/>
            </resource>
            <resource name="rm" location="commandlineTools">
                <argument id="file">
                    <value>^/Pipeline/parameters/parameter[name='4dfpdir']/values/unique/text()^</value>
                </argument>
                <argument id="r"/>
            </resource>
        </step>
        <!-- <step id="END-Notify" description="Notify" precondition="^count(/Pipeline/parameters/parameter[name='scanIds_loopInput']/values/list) >0^">
            <resource name="Notifier" location="notifications" >
                <argument id="user">
                    <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                </argument>
                <argument id="password">
                    <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                </argument>
                <argument id="to">
                    <value>^/Pipeline/parameters/parameter[name='useremail']/values/unique/text()^</value>
                </argument>
                <argument id="cc">
                    <value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
                </argument>
                <argument id="from">
                    <value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
                </argument>
                <argument id="subject">
                    <value>^concat('CNDA update: ', /Pipeline/parameters/parameter[name='label']/values/unique/text(),' T1 Atlas registered image(s) available')^</value>
                </argument>
                <argument id="host">
                    <value>^/Pipeline/parameters/parameter[name='mailhost']/values/unique/text()^</value>
                </argument>
                <argument id="body">
                    <value>^concat('Dear ',/Pipeline/parameters/parameter[name='userfullname']/values/unique/text(),',&lt;br&gt; &lt;p&gt;', /Pipeline/parameters/parameter[name='label']/values/unique/text(),' has been registered to the atlas ',/Pipeline/parameters/parameter[name='atlas']/values/unique/text(),'.&lt;/p&gt;&lt;br&gt; &lt;p&gt;Details for this session are available at &lt;a href="',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'data/experiments/',/Pipeline/parameters/parameter[name='id']/values/unique/text(),'?format=html"&gt;the CNDA website.&lt;/a&gt; &lt;/p&gt;&lt;br&gt;CNDA Team.')^
                    </value>
                </argument>
            </resource>
        </step> -->
    </steps>
</Pipeline>
