#!/usr/bin/python
from __future__ import division
import os, re, sys, json, argparse

versionNumber='5'
dateString='20140814'
author='flavin'
progName=os.path.basename(sys.argv[0])
idstring = '$Id: %s,v %s %s %s Exp $'%(progName,versionNumber,dateString,author)


def main():
    #######################################################
    # PARSE INPUT ARGS
    parser = argparse.ArgumentParser(description='Generate XML assessor file from PUP output logs')
    parser.add_argument('-v', '--version',
                        help='Print version number and exit',
                        action='version',
                        version=versionNumber)
    parser.add_argument('--idstring',
                        help='Print id string and exit',
                        action='version',
                        version=idstring)
    parser.add_argument('-o','--outfile',
                        help='Path to output file')
    parser.add_argument('-l','--logNameBase',
                        help='Base name of the PUP log file(s)')
    parser.add_argument('-a','--average',
                        help='Average left and right regions?')
    parser.add_argument('logs',nargs='+',
                        help='PUP log file(s) containing ROI statistics')
    args=parser.parse_args()

    logNameBase = args.logNameBase
    logList = args.logs
    doAvg = args.average=='True'
    #######################################################


    #######################################################
    # PARSE STAT LOGS
    # Build a stats dictionary.
    # Dict will be of the form {ROI:{StatTitle:StatNumberValue,...},...}
    logRe = re.compile(r'{}(_msum)?_(?P<type>[^_]*(_PVC2C|_RSF)?)\.txt'.format(logNameBase))

    statsDict = {}

    if len(logList)==1 and '*' in logList[0]:
        message = "%s ERROR: The shell could not expand the glob %s into a list of file names."%(progName,logList[0])
        message += "\nThese files usually contain the PUP stats, but in this case they do not exist."
        message += "\nWithout these files I cannot find any stats to report."
        sys.exit(message)

    for logFilePath in logList:
        # Get the stat type from the filename
        logFileName = os.path.basename(logFilePath)
        logStatsMatch = logRe.match(logFileName)
        if not logStatsMatch:
            message = "%s ERROR: Filename regex didn't match on log file %s"%(progName,logFileName)
            message += "\nThe filenames are used to determine the type of the statistics contained within each file."
            message += "\nIf I can't correctly parse the filename, I do not know what to do with the stats inside."
            sys.exit(message)
        logStatsTypeStr = logStatsMatch.group('type')

        # Remove the LR, if it is there
        logStatsTypeList = logStatsTypeStr.split('_')
        logStatsTypeList[0] = logStatsTypeList[0][:-2] if logStatsTypeList[0].endswith('LR') else logStatsTypeList[0]
        logStatsType = '_'.join(logStatsTypeList)

        print 'Reading from %s'%logFilePath
        addLogFileContentToStatsDict(statsDict,logFilePath,logStatsType)

    #######################################################

    #######################################################
    # AVERAGE LEFT/RIGHT STATS & CALCULATE MC
    # First, match up the left/right region names.
    # Then send the region pairs off to be averaged, and add the average to the statsDict
    # If all necessary regions are present, calculate mean cortical stats
    if doAvg:
        naiveStatsOfInterest = ['BP','SUVR','R2','INTC']
        statsOfInterest = [statTitle for statTitle in naiveStatsOfInterest if \
                    all([statTitle in roiStatsDict for roiStatsDict in statsDict.itervalues()])]
        if len(statsOfInterest) == 0:
            sys.exit("%s ERROR: I expected to find stat titles such as BP or SUVR, but I did not."%progName)

        leftRe = re.compile(r'([Ll]eft|lh|LH)[-_](?P<region>.*)')
        rightRe = re.compile(r'([Rr]ight|rh|RH)[-_](?P<region>.*)')
        lrMatchDict = {}
        for roi in statsDict:
            leftRoi = leftRe.match(roi)
            rightRoi = rightRe.match(roi)
            if leftRoi:
                reg = leftRoi.group('region')
            elif rightRoi:
                reg = rightRoi.group('region')
            else:
                continue
            if reg not in lrMatchDict:
                lrMatchDict[reg] = []
            lrMatchDict[reg].append(roi)

        for region,lrList in lrMatchDict.iteritems():
            if len(lrList) == 2:
                print 'Averaging {1} and {2} into {0}'.format('TOT_'+region,*lrList)
                statsDict['TOT_'+region] = weightedAverageStats(statsDict,lrList,statsOfInterest)

        MCRegions = ['TOT_gyrusrectus','TOT_lattemp','TOT_precuneus','TOT_prefrontal']
        if any([reg not in statsDict for reg in MCRegions]):
            print 'Not calculating Mean Cortical stats'
            print 'List of regions:\n'+'\n'.join(statsDict.keys())
            for reg in MCRegions:
                print 'Need these regions for MC calculation: '
                if reg not in statsDict:
                    print reg+' - Not in the list of regions. (Name must match exactly.)'
                else:
                    print reg+' - In the list of regions.'
        else:
            print 'Calculating Mean Cortical stats'
            statsDict['MC'] = averageStats(statsDict,MCRegions,statsOfInterest)
    #######################################################



    #######################################################
    # WRITE STATS TO FILE
    statsJSON = json.dumps(statsDict)
    print 'Writing stats to %s'%args.outfile

    with open(args.outfile,'w') as f:
        f.write(statsJSON)

def averageStats(statsDict,regionList,statsList):
    statsDictsOfInterest = [statsDict[reg] for reg in regionList]
    retDict = {stat:'{:.4f}'.format(sum([float(statsDict[stat]) for statsDict in statsDictsOfInterest])/len(statsDictsOfInterest)) for stat in statsList}
    retDict['NVox'] = str(sum([int(statsDict['NVox']) for statsDict in statsDictsOfInterest]))
    return retDict

def weightedAverageStats(statsDict,regionList,statsList):
    # Pull out the stats dicts for the regions we are averaging
    statsDictsOfInterest = [statsDict[reg] for reg in regionList]

    # Total number of voxels for regions we are averaging
    totVox = sum([int(statsDict['NVox']) for statsDict in statsDictsOfInterest])

    # For each stat, do a weighted average across the regions, weighted by number of voxels.
    retDict = {stat:'{:.4f}'.format(sum([float(statsDict[stat])*int(statsDict['NVox']) for statsDict in statsDictsOfInterest])/totVox) for stat in statsList}
    retDict['NVox'] = str(totVox)
    return retDict

def addLogFileContentToStatsDict(statsDict,logFilePath,logStatsType):
    # Open the log file and parse the stats
    with open(logFilePath,'r') as log:
        lines = log.read().splitlines()

        # First line of the file is titles
        # But we must change the formatting to make titles conform to db entries
        rawTitles = lines[0].split()[1:] # Don't need the first entry
        titles = fixTitles(rawTitles,logStatsType)

        # Subsequent lines are ROIs and stats
        for line in lines[1:]:
            roiAndStats = line.split()
            roi = roiAndStats[0]
            stats = roiAndStats[1:]

            # Get the roi dict from the big statsDict
            # Initialize to empty dict if not yet present
            if roi not in statsDict:
                statsDict[roi]={}
            roiDict = statsDict[roi]

            for i,stat in enumerate(stats):
                # NVox is in all files. If it is already in the statsDict,
                #   but doesn't have the same value that we see it has now,
                #   then something is wrong.
                if titles[i]=='NVox' and 'NVox' in roiDict and roiDict['NVox'] != stat:
                    message = '%s ERROR: Voxel number mismatch for roi %s. %s!=%s'%(progName,roi,roiDict['NVox'],stat)
                    message += "\nThis indicates either something went horribly wrong with the PUP scripts,"
                    message += "\nor I have not parsed the stats correctly."
                    sys.exit(message)
                else:
                    roiDict[titles[i]] = fixStats(stat)

def fixStats(stat):
    # Return values that fit the XML spec for floats
    if stat=="nan": return "NaN"
    if "inf" in stat: return "INF"
    return stat

def fixTitles(titles,logStatsType):
    # First, replace those titles we can pre-load in a dictionary
    titleDict = {'R^2':'R2','Intc':'INTC'}
    for logTitle,schemaTitle in titleDict.iteritems():
        if logTitle in titles:
            titles[ titles.index(logTitle) ] = schemaTitle

    if 'SUVR' in titles:
        # for SUVR stats, the title in the log is always just SUVR, but the statType
        # of the log file may be SUVR_something. We want to put that statType in the XML.
        titles[ titles.index('SUVR') ] = logStatsType
    elif '_' in logStatsType:
        # If there is an underscore in the log file stats type, and it isn't SUVR_something,
        # it will be model_something. We don't care about the model, so we dump it.
        # Then all the titles in the log should be named title_something in the XML.
        # (Except for number of voxels, which is always NVox.)
        model,statType = logStatsType.split('_')
        titles = [t+'_'+statType if t != 'NVox' else t for t in titles ]

    return titles

if __name__ == '__main__':
    print idstring
    main()
